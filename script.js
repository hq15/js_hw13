const switchTheme = document.getElementById("switch-theme");
const theme = document.getElementById("theme");
if (localStorage.getItem("data-theme") === "light") {
  theme.setAttribute("href", "css/light-styles.css");
} else {
  theme.setAttribute("href", "css/dark-styles.css");
}

switchTheme.onclick = function () {
  if (theme.getAttribute("href") == "css/dark-styles.css") {
    theme.href = "css/light-styles.css";
    switchTheme.innerHTML = "Change theme to dark";
    window.localStorage.setItem("data-theme", "light");
  } else {
    theme.href = "css/dark-styles.css";
    switchTheme.innerHTML = "Change theme to light";
    window.localStorage.setItem("data-theme", "dark");
  }
};
